package ar.edu.unqui.tpi.paises_android;

import android.content.Intent;
import android.databinding.BaseObservable;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;

import ar.edu.unqui.tpi.paises_android.Modelo.Pais;

public class ControllerDatos extends BaseObservable{

    private ActivityDatos activity;
    private Pais paisAMostrar;

    public ControllerDatos(ActivityDatos _activity){
        this.activity = _activity;
    }

    public void setPaisAMostrar(Pais _pais){
        this.paisAMostrar = _pais;
    }

    public String getNombre(){
        return this.paisAMostrar.getNombre();
    }

    public String getRegion(){
        return this.paisAMostrar.getRegion();
    }

    public String getPoblacion(){
        return this.paisAMostrar.getPoblacion();
    }

    public String getBandera(){
        //Intent intent = new Intent(Intent.ACTION_VIEW);
        //intent.setData(Uri.parse(this.paisAMostrar.getBandera()));
        //activity.startActivity(intent);
        return this.paisAMostrar.getBandera();
    }

    public String getCapital(){
        return this.paisAMostrar.getCapital();
    }

}
