package ar.edu.unqui.tpi.paises_android;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import ar.edu.unqui.tpi.paises_android.Tool.ApplicationToolset;
import ar.edu.unqui.tpi.paises_android.databinding.ActivityPaisesInicialBinding;

public class ActivityPaisesInicial extends AppCompatActivity {

    private ControllerPaisesInicial controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ApplicationToolset.setContext(this.getApplicationContext());

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paises_inicial);
        controller = new ControllerPaisesInicial(this);
        ActivityPaisesInicialBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_paises_inicial);
        binding.setController(controller);
    }
}
