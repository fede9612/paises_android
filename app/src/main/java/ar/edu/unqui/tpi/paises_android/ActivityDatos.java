package ar.edu.unqui.tpi.paises_android;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.PictureDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;


import com.bumptech.glide.RequestBuilder;

import ar.edu.unqui.tpi.paises_android.Modelo.Pais;
import ar.edu.unqui.tpi.paises_android.Tool.GlideApp;
import ar.edu.unqui.tpi.paises_android.Tool.MyAppGlideModule;
import ar.edu.unqui.tpi.paises_android.Tool.SvgSoftwareLayerSetter;
import ar.edu.unqui.tpi.paises_android.databinding.ActivityDatosBinding;


public class ActivityDatos extends AppCompatActivity{

    private ControllerDatos controller;
    private RequestBuilder<PictureDrawable> requestBuilder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        controller = new ControllerDatos(this);
        ActivityDatosBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_datos);
        binding.setController(this.controller);

        Intent paisRecibido = this.getIntent();
        Pais paisAMostrar = (Pais) paisRecibido.getSerializableExtra("pais");
        controller.setPaisAMostrar(paisAMostrar);

        ImageView imageView = findViewById(R.id.imagenBandera);
        ImageView imageLoading = findViewById(R.id.imagenLoading);

        imageLoading.setBackgroundResource(R.drawable.loading);
        AnimationDrawable frameLoading = (AnimationDrawable) imageLoading.getBackground();
        frameLoading.start();

        requestBuilder = GlideApp.with(this)
                .as(PictureDrawable.class)
                .listener(new SvgSoftwareLayerSetter());

        requestBuilder.load(controller.getBandera()).into(imageView);

    }

}
