package ar.edu.unqui.tpi.paises_android;

import android.content.Intent;
import android.databinding.BaseObservable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import ar.edu.unqui.tpi.paises_android.Modelo.Pais;
import ar.edu.unqui.tpi.paises_android.Tool.ApplicationToolset;

public class ControllerPaisesInicial extends BaseObservable{

    private AppCompatActivity activity;
    private String titulo = "Seleccione un Continente para más datos";

    public ControllerPaisesInicial(AppCompatActivity _activity){
        super();
        this.activity = _activity;
    }

    public String getTitulo(){
        return this.titulo;
    }

    public void mostrarContinente(String _continente){
        Intent saltoAPais = new Intent(this.activity.getBaseContext(), DatosDePais.class);
        saltoAPais.putExtra("regionSeleccionada", _continente);
        this.activity.getBaseContext().startActivity(saltoAPais);
    }

}
