package ar.edu.unqui.tpi.paises_android;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import ar.edu.unqui.tpi.paises_android.Tool.ApplicationToolset;
import ar.edu.unqui.tpi.paises_android.databinding.ActivityDatosPaisBinding;


public class DatosDePais extends AppCompatActivity {

    private ControllerDatosDeRegion controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ApplicationToolset.setContext(this.getApplicationContext());

        super.onCreate(savedInstanceState);
        String regionSeleccionada = this.getIntent().getStringExtra("regionSeleccionada");
        setContentView(R.layout.activity_datos_pais);
        controller = new ControllerDatosDeRegion(this);
        controller.setRegionSeleccionada(regionSeleccionada);

        ActivityDatosPaisBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_datos_pais);
        binding.setController(controller);
        this.agregarOnClick();
    }

    private void agregarOnClick() {
        ListView listaDeAnios = this.findViewById(R.id.PaisesIniciales);
        listaDeAnios.setOnItemClickListener((listView, viewFila, position, Id) -> {
            this.controller.mostrarPais(position);
        });
    }

}
