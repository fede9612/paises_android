package ar.edu.unqui.tpi.paises_android.Modelo;

import java.io.Serializable;

public class Pais implements Serializable{
    private String nombre;
    private String region;
    private int poblacion;
    private String bandera;
    private String capital;

    public Pais(String _nombre, String _region, int _poblacion, String _bandera, String _capital){
        this.nombre = _nombre;
        this.region = _region;
        this.poblacion = _poblacion;
        this.bandera = _bandera;
        this.capital = _capital;
    }

    public String toString() {
        return this.getNombre() + " de " + this.getRegion();
    };

    public String getRegion() {
        return region;
    }

    public String getNombre() {
        return nombre;
    }

    public String getPoblacion(){
        return String.valueOf(poblacion);
    }

    public String getBandera(){ return bandera; }

    public String getCapital(){ return capital; }
}
