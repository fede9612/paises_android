package ar.edu.unqui.tpi.paises_android;

import android.content.Intent;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ar.edu.unqui.tpi.paises_android.Modelo.Pais;
import ar.edu.unqui.tpi.paises_android.Tool.ApplicationToolset;

public class ControllerDatosDeRegion extends BaseObservable{

    private AppCompatActivity activity;
    private List<Pais> paises = new ArrayList<>();
    private ArrayAdapter<Object> paisesAdapter;
    private SimpleAdapter segundoAdapter;

    private String regionSeleccionada;


    public ControllerDatosDeRegion(AppCompatActivity _activity){
        super();
        this.activity = _activity;
        this.paisesAdapter = this.createPaisesAdapter();
        this.segundoAdapter = null;
        this.paisesAdapter.add("Cargando los datos ...");
        this.fetchPaises();
    }

    public void setRegionSeleccionada(String _regionSeleccionada) {
        this.regionSeleccionada = _regionSeleccionada;
    }

    public void createSegundoAdapter() {
        this.segundoAdapter = new SimpleAdapter(
                this.activity,
                this.crearMapasAMostrar(),
                android.R.layout.simple_list_item_2,
                new String[]{"nombre", "region"},
                new int[]{android.R.id.text1, android.R.id.text2}
        );
        this.notifyPropertyChanged(BR.paisesAdapter);
    }

    public List<Map<String, String>> crearMapasAMostrar() {
        ArrayList<Map<String, String>> losMapas = new ArrayList<>();
        for (Pais libro: paises) {
            Map<String, String> infoPais = new HashMap<>();
            infoPais.put("nombre", libro.getNombre());
            infoPais.put("region", libro.getRegion());
            losMapas.add(infoPais);
        }
        return losMapas;
    }

    @Bindable
    public ListAdapter getPaisesAdapter() {
        if (this.segundoAdapter == null) {
            return this.paisesAdapter;
        } else {
            return this.segundoAdapter;
        }
    }

    public ArrayAdapter<Object> createPaisesAdapter() {
        ArrayAdapter<Object> adapter = new ArrayAdapter<>(
                this.activity, android.R.layout.simple_list_item_1
        );
        return adapter;
    }

    public void fetchPaises() {
        String url = "https://restcountries.eu/rest/v2/all";

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest
                (Request.Method.GET, url,null,
                        (JSONArray response) -> {
                            this.procesarRespuestaDelServer(response);
                            this.llenarLibrosAdapter();
                        },
                        (VolleyError error) -> {
                            error.printStackTrace();
                            throw new RuntimeException("Error en el request REST", error);
                        }
                );

        ApplicationToolset.toolset().addToRequestQueue(jsonArrayRequest);
    }


    public void procesarRespuestaDelServer(JSONArray respuesta) {
        try {
            for (int indice = 0; indice < respuesta.length(); indice++) {
                JSONObject pais = respuesta.getJSONObject(indice);

                String nombre = pais.getString("name");
                String region = pais.getString("region");
                Integer poblacion = pais.getInt("population");
                String bandera = pais.getString("flag");
                String capital = pais.getString("capital");

                if(region.equals(this.regionSeleccionada)){
                    this.paises.add(new Pais(nombre, region, poblacion, bandera, capital));
                }
            }
        } catch (JSONException error) {
            error.printStackTrace();
            throw new RuntimeException("Error en el procesamiento del JSON", error);
        }
    }

    public void llenarLibrosAdapter() {
        this.paisesAdapter.clear();
        this.createSegundoAdapter();
    }

    public Pais getPais(int posicion){
        return this.paises.get(posicion);
    }

    public void mostrarPais(int posicion){
        Intent saltoDeActivity = new Intent(this.activity, ActivityDatos.class);
        saltoDeActivity.putExtra("pais",this.getPais(posicion));
        this.activity.startActivity(saltoDeActivity);
    }
}
